#!/usr/bin/env node
var program = require('commander')
var mongoose = require('mongoose')
var moment = require('moment')
mongoose.Promise = require('bluebird')

const User = require('./models/User')
const Certificate = require('./models/Certificate')
const fs = require('fs')


const image_downloader = require('image-downloader')

program
 .arguments('<file>')
 .option('--connection <mongodb://username:password@host:port/database>', 'The db host and port')
 .option('--file <filename>', 'The name of the generated csv file.')
 .option('--csvpath <path to csv>', 'path to csv file')
 .option('--certificateDir <path to certificateDir>', 'path to certificateDir')
 .option('--s3Url <url to s3 server>', 'url to s3 server')
 .option('--days <number of days back>', 'days back to grab files')
 .action(function() {

 })
 .parse(process.argv)

const dbUrl = program.connection || 'mongodb://localhost:27017/montivendor-dev';

  mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + dbUrl);
    console.log('Selecting ' + moment().subtract(program.days, 'days').calendar())
  });

  const greater = program.days ? { downloaded: { '$gte': moment().subtract(program.days, 'days').toDate() } }
    : { downloaded: { '$eq': null } }

  mongoose.connection.once('open', () => {
    Certificate
      .find(greater)
      .exec((err, certificates) => {
        console.log('Exporting ' + certificates.length + ' certificates...')
        if (err) {
          console.log('Error ', err)
        } else {
          if (certificates.length === 0) {
            mongoose.connection.close()
          }
          certificates.map( (cert, counter) => {
            var options = {
                url: program.s3Url + cert.url,
                dest: program.certificateDir,
                done: function(err, filename, image) {
                    if (err) {
                        throw err;
                    }
                },
            }
            image_downloader(options)
            cert.downloaded = Date.now()
            cert.save().finally(
              () => {

                if (counter + 1 >= certificates.length) {
                  console.log('Done!')
                  mongoose.connection.close()
                }
              })
          })
        }
      })
      .then(function(docs) {
          Certificate.csvReadStream(docs)
            .pipe(fs.createWriteStream(program.csvpath + program.file, { 'flags': 'a' }));
      })
  })

  mongoose.connect(dbUrl);

  // If the Node process ends, close the Mongoose connection
  process.on('SIGINT', function() {
    mongoose.connection.close(function () {
      console.log('Mongoose default connection disconnected through app termination');
      process.exit(0);
    });
  });
