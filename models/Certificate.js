'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseToCsv = require('mg-to-csv')

const certificateModel = new Schema({
  url: { type: String, required: true },
  poNumber: { type: String, required: true },
  date: { type: Date, default: Date.now },
  downloaded: { type: Date, required: false }
});

certificateModel.plugin(mongooseToCsv, {
  headers: 'poNumber url',
  constraints: {
    'poNumber': 'poNumber',
    'url': 'url'
  },
  terminator: '\r\n'
});

module.exports = mongoose.model('Certificate', certificateModel);
